package com.example.mamadian.tp1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by mamadian on 29/01/16.
 */
public class Affichage extends AppCompatActivity {
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affichage);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Intent intent = getIntent();

        Model personne = intent.getExtras().getParcelable("personne");
        TextView t1 = (TextView) findViewById(R.id.textView1);
        t1.setText(personne.getName());
        TextView t2 = (TextView) findViewById(R.id.textView2);
        t2.setText(personne.getPrenom());
        TextView t3 = (TextView) findViewById(R.id.textView3);
        t3.setText(personne.getDate());
        TextView t4 = (TextView) findViewById(R.id.textView4);
        t4.setText(personne.getVille());
        TextView t5 = (TextView) findViewById(R.id.textView5);
        t5.setText(personne.getDpt());
    }
}
